package com.span.ranking;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileTextProcessator extends TextProcessator {

	public FileTextProcessator(String fileName) {
		super(fileName);
	}

	@Override
	protected String getText() {
		return getStringFromFile( getFileName() );
	}

	private String getFileName() {
		return text;
	}

	@Override
	protected String getSeparator() {
		return System.lineSeparator();
	}


	@SuppressWarnings("resource")
	private String getStringFromFile(String file) {
	    StringBuffer text = new StringBuffer();
	    
	    try {
	    	FileInputStream fileStream = new FileInputStream( file );
		    BufferedReader br = new BufferedReader( new InputStreamReader( fileStream ) );
			for ( String line; (line = br.readLine()) != null; )
			    text.append( line + System.lineSeparator() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return text.toString();
	}
}
