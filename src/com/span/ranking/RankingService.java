package com.span.ranking;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.*;  

public class RankingService{

	static final Integer PROCESS_WITH_FILE = 1;
	static final Integer PROCESS_WITH_TEXT = 2;

	private TextProcessator textProcessator;

	private Map<String,Integer> rankingUnsorted = new HashMap<String, Integer>();
	private Map<String,String> concatRankingUnsorted = new HashMap<String, String>();
	LinkedHashMap<String, String> rankingSorted; 
	
	public RankingService(String text, Integer typeProcess) throws Exception {
		textProcessator = TextProcessator.getTextProcessator(text, typeProcess);
	}

	public String getCalculateRanking() {
		List<String> games = textProcessator.getScores();
		return processGames(games);
	}
	
	private String processGames(List<String> games) {
		for(String g: games) {
			String[] teams = g.split(",");
			addToScore(teams[0].trim(),teams[1].trim());
		}
		concatMap();
		sortMap();
        StringBuilder ranking = new StringBuilder();
		int i = 1 ;
		String rankTemp = "-1";
		List<String> scoreTemps = new ArrayList<String>();
		Integer index = 0;
		boolean b = true;
		for (Map.Entry<String,String> entry : rankingSorted.entrySet()){
			if(b) {
				rankTemp = entry.getValue().split("-")[0];
				b = false;
			}
			if(!rankTemp.equals(entry.getValue().split("-")[0])) {
				
				scoreTemps.sort(String::compareToIgnoreCase);    
				for(String sc: scoreTemps) {
					ranking.append(sc+"\n");
				}
				scoreTemps = new ArrayList<String>();
				i++;
				rankTemp = entry.getValue().split("-")[0];
				
			}
			scoreTemps.add(i + ". " + entry.getValue().split("-")[1].trim() + ", " + entry.getValue().split("-")[0]+ "pt"+(entry.getValue().split("-")[0].equals("1")?"":"s"));
			index++;
			if( index.equals(rankingSorted.size())) {
				scoreTemps.sort(String::compareToIgnoreCase);    
				for(String sc: scoreTemps) {
					ranking.append(sc+"\n");
				}
			}
		}
		return ranking.toString();
	}

	private void concatMap() {
		for (Map.Entry<String,Integer> entry : rankingUnsorted.entrySet()){
			if(!concatRankingUnsorted.containsKey(entry.getValue()+"-"+entry.getKey())){
				concatRankingUnsorted.put(entry.getValue()+"-"+entry.getKey(), entry.getValue()+"-"+entry.getKey());
			}
		}
	}

	private void addToScore(String team1, String team2) {

		String rest1S = getStringFromRegex(team1);
		Integer rest1I = getIntegerFromRegex(team1);
		String rest2S = getStringFromRegex(team2);
		Integer rest2I = getIntegerFromRegex(team2);
		addTeamResultToScore(rest1S,rest1I,rest2I);
		addTeamResultToScore(rest2S,rest2I,rest1I);
	}

	private String getStringFromRegex(String string) {
		Pattern p = Pattern.compile("([a-zA-z ]+)([0-9]+)");
		Matcher m = p.matcher(string);
		m.find();
		return m.group(1);
	}

	private Integer getIntegerFromRegex(String string) {
		Pattern p = Pattern.compile("([a-zA-z ]+)([0-9]+)");
		Matcher m = p.matcher(string);
		m.find();
		return Integer.parseInt(m.group(2));
	}
	
	private void addTeamResultToScore(String teamName, Integer scoreTeamInt, Integer scoreTeamVsInt) {
		Integer score = getScoreFromResults(scoreTeamInt,scoreTeamVsInt);
		assignScoreToMap(teamName,score);
		
	}

	private void sortMap() {
		rankingSorted = new LinkedHashMap<>();
		concatRankingUnsorted.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
	        .forEachOrdered(x -> rankingSorted.put(x.getKey(), x.getValue()));
	}

	private Integer getScoreFromResults(Integer scoreTeamInt, Integer scoreTeamVsInt) {
		Integer score = 1;
		if(scoreTeamInt>scoreTeamVsInt) {
			score = 3;
		}else if(scoreTeamInt<scoreTeamVsInt) {
			score = 0;
		}
		return score;
	}

	private void assignScoreToMap(String teamName, Integer score) {

        if (rankingUnsorted.containsKey(teamName)) {
            score += rankingUnsorted.get(teamName);
        }

        rankingUnsorted.put(teamName, score);
    }

	public static String getTypeName(Integer type) {
		if(type.equals(PROCESS_WITH_FILE)) {
			return "nombre de archivo";
		}else if(type.equals(PROCESS_WITH_TEXT)) {
			return "texto separado por @";
		}
		return "";
	}
}
