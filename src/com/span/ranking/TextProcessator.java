package com.span.ranking;

import java.util.List;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

abstract class TextProcessator {

	protected abstract String getText();
	protected abstract String getSeparator();
	String text;
	
	public TextProcessator(String text) {
		this.text = text;
	}
	
	protected List<String> getScores(){
		List<String> scores = Stream.of(getText().trim().split(getSeparator())).collect(toList());
		return scores;
	}
	
	
	static TextProcessator getTextProcessator(String text, Integer typeProcess) throws Exception {
		switch(typeProcess) {
			case 1:
				return new FileTextProcessator(text); 
			case 2:
				return new StringTextProcessator(text);
			default:
				throw new Exception("Type process not defined");
		}
	}

}
