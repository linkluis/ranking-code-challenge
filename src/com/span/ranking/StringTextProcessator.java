package com.span.ranking;

public class StringTextProcessator extends TextProcessator {
	
	public StringTextProcessator(String text) {
		super(text);
	}

	@Override
	protected String getText() {
		return text;
	}

	@Override
	protected String getSeparator() {
		return "@";
	}

}
