package com.span.ranking;

public class RankingController {
	
	private RankingService rankingService;
	
	public RankingController(RankingService rankingService) {
		super();
		this.rankingService = rankingService;
	}

	public String getCalculateRanking() {
		return rankingService.getCalculateRanking();
	} 
}
