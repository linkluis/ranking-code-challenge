package com.span.ranking;

import java.io.*;

public class MainCalculateRanking {
	public static void main(String[] args) throws Exception {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));	        
	 	String scoreResult;
	 	if(args==null || args.length<1) {

	        System.out.println("Selecciona una opcion:");
	        String type = "";
	 		while(!type.equals("1") && !type.equals("2")){
		        System.out.println("1- Archivo");
	 			System.out.println("2- Texto separados por @");
		 		type = bufferedReader.readLine().trim();
	 		}
	 		System.out.println("Ingresa el "+RankingService.getTypeName(Integer.parseInt(type)));
	 		String text = bufferedReader.readLine().trim();
	 		scoreResult = new RankingController(new RankingService(text,Integer.parseInt(type))).getCalculateRanking();
	 	}else {
	 		String nameFile = args[0];
	 		scoreResult = new RankingController(new RankingService(nameFile,RankingService.PROCESS_WITH_FILE)).getCalculateRanking();
	 	}
        System.out.print(scoreResult);
			
	}
}
